# anita-shopping-list

A frictionless shopping list for couples

When you are doing the shopping:
* No quiero desesperarme haciendo la lista
* No quiero perder el tiempo usando la lista

A shopping list is just an assisted notepad first, then an assisted calculator.
Many created managed stock applications, tools that make you feel miserable using them.
This is just a tool that let you perform faster than using a notepad and a calculator.

Current apps fail miserably in fulfilling those 2 tenants, after forcing you to use them for many days you stop ussing them.
* You don't want to think where the option is hidden in the app
* You have an app but a few items are just not on it since there is a lot of frictino and during the shopping, update the list takes ages and mental energy

Las applicaciones actuales son tan malas q la gente deja de usarlas,
¿Cuánta gente ves usando una app por el centro comercial?
Estoy seguro q son un éxito como startups, pero ni los creadores las usan.
Tendrán millones de descargas, pero recurrencia de usuarios 0 después del primer o segundo usage
Las has usado? por cuanto :D?


* Objectives
** The app must be faster than keeping a list in a text editor (reusing the previous written items)
** The app must let the user has a clear prices sum result (some shopping centers have mislabeled products)
** The app must do the calculation of the prices in the final shopping list, as the user checks in products (a medida q los marca como q están comprados)
** The shopping list may have the products in advace or not, may be the products are added during the shopping itself, very easy UI

* Despensa
** Compartida entre usuarios, tiempo real, tiene q ser extremadamente rápida en la sincronización, porque 2 personas pueden estar haciendo la compra a la vez y es extremadamente fustrante no ver las actualizaciones
** En la que se meten los productos que vamos a usar en la lista de la compra del dia posterior mente
** La despensa tiene que recordar productos que se introducen en las listas de la compra q no habían sido introducidos en la despensa
** La despensa se muestra alfabéticamente
** La despensa se sincroniza entre móviles instantáneamente, al guardar y modificar

* Lista de la compra
* Pantalla de check in




* Usabilidad
** Añadir y quitar productos de la lista en caliente (durante la compra) tiene q ser extremadamente simple
** UI has to have clear space for big fingers to click
** UI will use flip screen to swap between lists (this may collide with the delete and addition features)



** con el objetivo de no tener q escribirlos luego cuando se está preparando la lista de la compra para el día.


![raw design 1](https://github.com/franferri/anita-shopping-list/blob/master/IMG_20190518_113106.jpg "raw design 1")

![raw design 2](https://github.com/franferri/anita-shopping-list/blob/master/IMG_20190518_113111.jpg "raw design 2")

![raw design 3](https://github.com/franferri/anita-shopping-list/blob/master/IMG_20190518_113120.jpg "raw design 3")

----






** (v2) La despensa se puede organizar (categorías):
*** Por zonas del super mercado
**** Fruta y verdura
**** Bebidas
**** Beauty, higiene, personal care
**** Kitchen and cleaning
**** Comestibles
**** Frozen
*** O por el criteri q el usuario quiera


** (v2) pantalla previa de listas
Una lista de "listas de la compra", q cada una tiene su "despensa" y "shopping list".
la despensa, en la aplicación de multilistas se puede marcar como compartida para que los elementos de una aparezcanen las otras listas, sin tener q recrear las despensas.


** (v2) las compras hechas, al final la app te da el precio final, pero cuando pagas has descuentos, vouchers de 10€ por ejemplo, si la pantalla de check in te deja añadirlo, te dará el precio final final.
O podemos tener un "elemento de la lista" llamado descuento, que al añadirlo te lista los precios y puedes añadir uno de 10€, de 5€ etc, así en la misma lista tienes aplicado el descuento

** (v2) las listas se guardan en el servidor en internet y luego en la web puedes revisarlas.





Notas en sucio:
La lista de la compra. Los packs son "precios", en clickear un elemento, abre la lista de sus precios, q tiene nombre y precio, el nombre pones el pack 3x2 3€, porque cuando buscas, buscas pan, no pan pack 3x2.
Buscas pan, y en los precios te salen los packs



Al añadir, q al tacharse te deje cambiar la cantidad


En el desplegable q sale al buscar q saque los precios, de forma q si tienes dado de alta lo mismo con diferentes precios, ves y elijes el q te funciona y no estas con mierdas una hora mientras compras y añades



La lista tiene un "antes de ir" y un " mientras compras. Los 2 modos son criticos







# during axure design, ideas:

En la pantalla de la lista de la compra, cuando le das al checkbox, no desaparece inmediatamente, te da 5 segundos para rectificar. Se hace gris y a los 5 segundos se va al carro, pero te deja desmarcarlo, por si fue un error

en la pantalla de la compra, a lo mejor tenemos añadido el pack de agua pero vamos y hay un 3x2, en vez de borrar y añadir el nuevo, poder transmutar/cambiar del q habíamos añadido al nuevo typo solo seleccionándolo mejoraría mucho la experiencia


Las sicronizaciones, tienen q ser en cada acción, tanto para enviar al servidor como para recibir del servidor los elementos


En la despensa el campo de búsqueda/añadir hace lo siguiente:
- Caso de uso 1, tenemos la depensa muy llena e ir a mano a por productos y subproductos es un jaleo, usamos el buscador, escribimos: Chicken noodles, y simplemente se filtra la pantalla principal, nos muestra todos los elementos q contengan chicken noodles, de forma que podemos añadirlos directamente a la lista de la compra

- Caso de uso 2, queremos añadir a la despensa un nuevo elemento (y a la lista de la compra), al escribir, nos aparecerán los filtrados y un item vacío adicional que nos permite guardar. Al gardar no se resetea el filtro, haciéndonos tener q reescribir, sino que aparece lo añadido como refrescando la pantalla, y ya podemos borrarlo y añadirlo de nuevo o enviarlo al carrito directamente (puesto q al añadirlo aparece como elemento normal y está el boón de enviar a la compra). De este modo el campo de búsqueda funciona exactametne igual en la compra y en la pantry