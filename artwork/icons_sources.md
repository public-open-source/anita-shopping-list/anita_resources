Exported the following SVG to PNG using this website: https://ezgif.com/svg-to-png
Export size: 500px x 500px

* Android icon
** https://visualpharm.com/free-icons/android-595b40b65ba036ed117d2899
iPhone icon
** https://visualpharm.com/free-icons/iphone-595b40b65ba036ed117d2e81

* Multiple devices (resources)
** https://visualpharm.com/free-icons/multiple%20devices-595b40b65ba036ed117d3009

* Backend 1
** https://visualpharm.com/free-icons/web%20design-595b40b65ba036ed117d3628
* Backend 2
** https://visualpharm.com/free-icons/web%20design-595b40b85ba036ed117da5e1

*Commercial web
** https://visualpharm.com/free-icons/web%20design-595b40b75ba036ed117d6dd6

* Business / Management
** https://visualpharm.com/free-icons/business-595b40b65ba036ed117d125d

* Keystore
** https://visualpharm.com/free-icons/certificate-595b40b65ba036ed117d3a3e

* Anita Shopping List
** https://www.flaticon.com/free-icon/shopping-cart_1168660