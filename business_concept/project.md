The project needs

Recording the time invested on it.
	* So far the concept was born one saturday day after an unsuccessfull experience with other apps in friday doing real shopping
	* Google spread sheet will serve for this
	
Android App
	* git repo
	* Continuous delivery to google drive shared folder + mail with link to the file to fast update
	* dev environment
	* google developer account

BackEnd api (shared lists, remember lists)
	* git repo
	* continuous deployment
	* hosting (could be the local intel nuc)

-- screens not related with the shopping --
* loading screen
* Usage policy
* Licenses (icons for example)
* App settings (select language)

-- artwork --

free only requires mention the author in the credits
* https://www.flaticon.com/free-icon/shopping-cart_1168660
* https://www.kisspng.com/png-shopping-bags-trolleys-personal-shopper-computer-i-3600458/
* http://chittagongit.com/icon/shopper-icon-11.html

-- Comertial phase --

website (not needed till we decide to go commertial)
	* domain
	* hosting

Business model can be
	* 100% free app 1 list, no advertisements at all
	* Shared list feature +1€ (gives you 2 members list)
	* +1€ gives you unlimited members in the list
	* +1€ gives you unlimited lists



Decent website with videos explaining how to operate, and comparing each usecase with others available.

Also the web must make the points like
* Many shop lists apps, do you see anybody using them? no. Designed to raise capital and accumulate downloads, but not being reused at all. Download and forget.
* They accumulate nonsense features like receipes, great cheeffs advises, advertisement, never working features like integration with super markets
* between the hits we have, shared lists that syncronize every 20 seconds or never, broken search apis, unfinished and unaccurate catalogs that makes you loose time when they suggest you items, 6 clics/taps to achive something that you can do in a notepad in 1, partial counters for totals or quantities,... they are basically stock managers tools

* Our app has been designed only for being faster than a notepad and a calculator. Just with that in mind, reusing your effort.



Splash screen with funny messages, like discord app does




(v2)
la pantalla final, si el usuario no ha puesto precios, no te molesta con totales.
si el usuario ha puesto precios te saca los totales y cantidad de elementos en el carrito (cantidad en gris pequeñito, no aporta mucho, por el sake de simplicidad quizás ni ponerlo)
PERO si tiene categorías para los productos, te saca el total total y el total por categoría, los productos agrupados por categorías. DE FORMA que ves en q categoría te estás gastando la pasta.

Al final q haya una opción (botón quizás) q te diga, mostrar todo por categorías y aplica a todas las pantallas, para evitar el mareo al swapear entre pantallas, todo tiene el mismo orden.
la pantalla de checkout muestra todo por categorías o alfabéticamente... pero te puede mostrar los costes por categorías si quieres.

las categorías es un añadido, la vista por defecto q sea la extremadamente simple. 
Si activamos categorías nos aparece el botón de ordenar alfabéticamente o por categorías
Si activamos categorías la pantalla de checkin q muestre el botón de alfabéticamente o por categorías como las otras, pero los totales tiene q cambiar también (al lado de cada categoría lo q hemos gastado en ella)





La pantalla de checkin, tiene buscador y deja buscar pero solo desmarcar para devolver a la lista de la compra algo.
El caso de uso es, has comprado algo, salmón, pero al llegar a la caja hay un mostrador con el salmón de otra marca más barao, de oferta, pero tu lista de checkout es enorme, buscar el salmón con el buscador es mas fácil q haciendo scroll a mano.
La lista de checkout desmete del carro solo, no te deja hacer nada más para evitar q se monte un lío cuando estamos haciendo la lista.

Evitar botones que hacen 2 o 3 funciones a la vez




Al pasar de la lista de la compra al carro q la aplicación haga el ruído de la caja registradora. (gamification)

Que al abrir la app, pite como la caja registradora al pasar los productos, pero 2 veces.





(comercial)
Explicar el problema q resolvemos, (simplificar un tiempo gastado recurrente)
* En la web comercial video donde comparamos tiempo tomando notas con un bloc de notas y cambiando a calculadora, en cada compra es lo mismo, es lineal. (hacer la lista lleva x segundos, marcarla por el centro comercial y actualizarla, añadir a la calculadora, al hacer la lista, puesto q la borramos puede q olvidemos cosas)
* En nuestra app, la primera compra cuesta lo mismo en tiempo, la segunda ya tenemos casi todo hecho, la tercera el tiempo baja al mínimo

Eso eslo q realmente vendemo, conveniencia








en vez de usar google notifications, hacer q si la app está activa hable con nuestro servidor cada 2 segundos para actualizarse.
Cuando la app se actualiza del online q haga un efecto visual (destello), como indicando q algo ha cambiado, de forma q se vea q mientras está abierta, reacciona casi instantánea.
Notificar q se ha actualización puede ser un destello o sonido o vibración, o mensaje de los que aparecen y se van donde dice: "fulanite ha hecho esto: xyz..."







Other lists in the store are stock management tools, not ready for the real usecases friction the user has when is doing the shopping itself. How many people you see using them? ;) ;)
Other lists are just so simple than they dont account per money or they do it poorly, so they basically save you of writting dow the list everytime, but not the calculator, if you are interested in the totals to check if the shopping center is ripping you off (or they did an inocent mistake).

Anita is JUST a better way than using a notepad and a calculator, than improves over time your shopping experience.